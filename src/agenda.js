const ScheduleService = require('./services/ScheduleService')
const Agenda = require('agenda')

const agenda = new Agenda({
  db: {
    address: process.env.MONGODB_URI || 'mongodb://localhost:27017/test'
  }
})

agenda.define('check emails', async () => {
  ScheduleService.checkEmails()
  console.log('check emails')
})

;(async function () {
  await agenda.start()
  await agenda.every('1 minutes', 'check emails')
}())
