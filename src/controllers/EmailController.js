const EmailService = require('../services/EmailService')

class EmailController {
  send (req, res) {
    const { subject } = req.body
    return EmailService.sendEmail(subject)
  }

  getAll (req, res) {
    EmailService.getAll().then(emails => {
      res.send(emails)
    })
  }

  create (req, res) {
    EmailService.create(req.body).then(() => {
      res.sendStatus(200)
    })
  }

  remove (req, res) {
    EmailService.remove(req.params.id).then(() => {
      res.send(201)
    })
  }
}

module.exports = new EmailController()
