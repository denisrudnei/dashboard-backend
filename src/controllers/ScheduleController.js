const ScheduleService = require('../services/ScheduleService')

class ScheduleController {
  create (req, res) {
    ScheduleService.create(req.body).then(() => {
      res.sendStatus(200)
    })
  }

  getAll (req, res) {
    ScheduleService.getSchedules().then(schedules => {
      res.send(schedules)
    })
  }

  remove (req, res) {
    ScheduleService.remove(req.params.id).then(() => {
      res.sendStatus(201)
    })
  }
}

module.exports = new ScheduleController()
