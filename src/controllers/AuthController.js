const AuthService = require('../services/AuthService')

class AuthController {
  create (req, res) {
    const { email, name, password } = req.body
    AuthService.register({
      email,
      name,
      password
    }).then(user => {
      res.send(user)
    }).catch(() => {
      res.status(400).json({
        message: 'Register failed'
      })
    })
  }

  login (req, res) {
    AuthService
      .login(req.body.email, req.body.password)
      .then(user => {
        req.session.authUser = user
        res.send(user)
      })
      .catch(() => {
        res.status(401).json({
          message: 'Failed to login'
        })
      })
  }

  logout (req, res) {
    delete req.session.authUser
    res.sendStatus(201)
  }
}

module.exports = new AuthController()
