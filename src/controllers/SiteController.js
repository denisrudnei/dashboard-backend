const SiteService = require('../services/SiteService')
const CookieService = require('../services/CookieService')

class SiteController {
  index (req, res) {
    SiteService.getSites().then(sites => {
      res.send(sites)
    })
  }

  create (req, res) {
    SiteService.create(req.body).then(site => {
      res.send(site)
    })
  }

  reload (req, res) {
    SiteService.reload(req.params.id).then(response => {
      res.set('Content-Type', 'image/jpeg')
      res.end(response)
    }).catch(() => {
      res.redirect('/static/notFound.png')
    })
  }

  async uploadCookies (req, res) {
    const data = req.files.cookie.data.toString()
    const cookies = CookieService.parse(data)
    await CookieService.setCookies(cookies)
    res.sendStatus(201)
  }

  getCount (req, res) {
    CookieService.getCount().then(result => {
      res.send(result)
    }).catch(err => {
      res.status(500).send(err)
    })
  }

  remove (req, res) {
    SiteService.remove(req.params.id).then(() => {
      res.sendStatus(201)
    })
  }
}

module.exports = new SiteController()
