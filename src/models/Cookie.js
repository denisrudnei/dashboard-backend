const { Schema, model } = require('mongoose')

const CookieSchema = new Schema({
  domain: {
    type: Schema.Types.String,
    required: [true, 'Value required']
  },
  path: {
    type: Schema.Types.String,
    required: [true, 'Value required']
  },
  name: {
    type: Schema.Types.String,
    required: [true, 'Value required']
  },
  value: {
    type: Schema.Types.String,
    required: [true, 'Value required']
  }
})

module.exports = model('Cookie', CookieSchema)
