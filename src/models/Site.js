const { Schema, model } = require('mongoose')

const SiteSchema = new Schema({
  url: {
    type: Schema.Types.String,
    required: [true, 'Required']
  }
})

module.exports = model('Site', SiteSchema)
