const { Schema, model } = require('mongoose')

const EmailSchema = new Schema({
  address: {
    type: Schema.Types.String
  }
})

module.exports = model('Email', EmailSchema)
