const { Schema, model } = require('mongoose')
const bcrypt = require('bcryptjs')

const UserSchema = new Schema({
  name: {
    type: Schema.Types.String,
    required: [true, 'Name required']
  },
  email: {
    type: Schema.Types.String,
    required: [true, 'Email required'],
    unique: true
  },
  password: {
    type: Schema.Types.String,
    required: [true, 'Password required']
  },
  role: {
    type: Schema.Types.String,
    default: 'user'
  }
})

UserSchema.pre('save', function () {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password)
  }
})

module.exports = model('User', UserSchema)
