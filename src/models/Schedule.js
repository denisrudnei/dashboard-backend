const { Schema, model } = require('mongoose')

const ScheduleSchema = new Schema({
  time: {
    type: Schema.Types.Date
  }
})

module.exports = model('Schedule', ScheduleSchema)
