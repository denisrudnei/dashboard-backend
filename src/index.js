const express = require('express')
const app = express()
const routes = require('./routes')
require('./db')
require('./agenda')

app.use(routes)
app.listen(process.env.PORT || 3000, () => {
  console.log('Server initialized')
})
