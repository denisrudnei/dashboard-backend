const nodemailer = require('nodemailer')
const SiteService = require('./SiteService')
const Email = require('../models/Email')

const transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  secure: false,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD
  }
})

class EmailService {
  async getAll () {
    return Email.find()
  }

  create (email) {
    return Email.create({
      address: email.address
    })
  }

  async sendEmail (subject) {
    const emails = await this.getAll()
    const to = emails.map(email => email.address)
    const sites = await SiteService.getSites()
    const images = {}
    let body = '<div>'

    for (const index in sites) {
      const site = sites[index]

      images[site._id] = await SiteService.reload(site._id)

      body += `<h2>${site.url}</h2><br /><img src="cid:${site._id}"/>`
    }

    body += '</div>'

    const attachments = await Promise.all(sites.map(async site => {
      return ({
        filename: `${site.url}.png`,
        content: images[site._id],
        cid: site._id.toString()
      })
    }))

    const info = await transporter.sendMail({
      from: process.env.MAIL_USER,
      to,
      subject,
      html: body,
      attachments
    })
    console.log(info)
  }

  async remove (id) {
    return Email.deleteOne({
      _id: id
    })
  }
}

module.exports = new EmailService()
