const Site = require('../models/Site')
const puppteer = require('puppeteer')
const CookieService = require('../services/CookieService')

class SiteService {
  constructor () {
    puppteer.launch({
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
      defaultViewport: {
        width: 1920,
        height: 1080
      }
    }).then(async browser => {
      this.browser = browser
    })
  }

  async getSites () {
    return Site.find()
  }

  async create (site) {
    return Site.create({
      url: site.url
    })
  }

  remove (id) {
    return Site.deleteOne({
      _id: id
    })
  }

  async reload (id) {
    const { url } = await Site.findOne({ _id: id })
    console.log(`Reloading ${url}`)
    const page = await this.browser.newPage()
    const cookies = await CookieService.getCookies()
    for (const index in cookies) {
      const { domain, path, name, value } = cookies[index]
      const cookie = { domain, path, value, name }
      try {
        await page.setCookie(cookie)
      } catch (e) {
        console.log(e)
      }
    }
    await page.goto(`http://${url}`)
    const buffer = await page.screenshot()
    page.close()
    console.log(`page ${url} captured`)
    return buffer
  }
}

module.exports = new SiteService()
