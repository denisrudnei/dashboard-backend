const Schedule = require('../models/Schedule')
const EmailService = require('./EmailService')
const { format } = require('date-fns')
const { zonedTimeToUtc } = require('date-fns-tz')
class ScheduleService {
  async getSchedules () {
    return Schedule.find()
  }

  create (schedule) {
    return Schedule.create({
      time: schedule.time
    })
  }

  async remove (id) {
    return Schedule.deleteOne({
      _id: id
    })
  }

  checkEmails () {
    this.getSchedules().then(schedules => {
      schedules.forEach(schedule => {
        console.log(schedule)
        if (format(zonedTimeToUtc(schedule.time), 'HH:mm') === format(zonedTimeToUtc(Date.now()), 'HH:mm')) {
          EmailService.sendEmail(schedule.time)
        }
      })
    })
  }
}

module.exports = new ScheduleService()
