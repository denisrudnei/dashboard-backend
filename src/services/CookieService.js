const Cookie = require('../models/Cookie')

class CookieService {
  parse (data) {
    const lines = data.split('\n').filter(line => {
      if (line.length === 0) return false
      return line[0] !== '#'
    })
    const obj = lines.map(line => {
      return {
        domain: line.split('\t')[0] || '',
        path: line.split('\t')[2] || '',
        name: line.split('\t')[5] || '',
        value: line.split('\t')[6] || ''
      }
    })
    return obj
  }

  clearCookies () {
    return Cookie.deleteMany({})
  }

  async setCookies (cookies) {
    await this.clearCookies()
    const cookiesPromises = cookies.map(cookie => {
      return this.create(cookie)
    })
    return Promise.all(cookiesPromises)
  }

  async getCount () {
    return new Promise((resolve, reject) => {
      Cookie.aggregate([
        {
          $group: {
            _id: '$domain',
            total: {
              $sum: 1
            }
          }
        }
      ], (err, result) => {
        if (err) return reject(err)
        return resolve(result)
      })
    })
  }

  create (cookie) {
    return Cookie.create(cookie)
  }

  getCookies () {
    return Cookie.find()
  }
}

module.exports = new CookieService()
