const User = require('../models/User')
const bcrypt = require('bcryptjs')

class AuthService {
  async login (email, password) {
    const user = await User.findOne({
      email: email
    })
    if (user === null) {
      Promise.reject(new Error('Login failed'))
    }
    const logged = bcrypt.compareSync(password, user.password)
    if (logged) return Promise.resolve(user)
    return Promise.reject(new Error('Login failed'))
  }

  async register (user) {
    return User.create({
      email: user.email,
      name: user.name,
      password: user.password
    })
  }
}

module.exports = new AuthService()
