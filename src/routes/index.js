const express = require('express')
const acl = require('express-acl')
const session = require('express-session')
const router = express.Router()
const bodyParser = require('body-parser')
const helmet = require('helmet')
const cors = require('cors')
const fileUploader = require('express-fileupload')
const SiteController = require('../controllers/SiteController')
const EmailController = require('../controllers/EmailController')
const ScheduleController = require('../controllers/ScheduleController')
const AuthController = require('../controllers/AuthController')

router.use(cors({
  credentials: true,
  origin: (_, callback) => {
    return callback(null, true)
  }
}))

router.use(fileUploader())

router.use('/static', express.static('public'))

router.use(bodyParser.json())
router.use(helmet())

router.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true
}))

acl.config({
  filename: 'nacl.json',
  roleSearchPath: 'session.authUser.role',
  defaultRole: 'guest'
})

router.use(acl.authorize)

router.get('/url', SiteController.index)
router.post('/url', SiteController.create)
router.get('/reload/:id', SiteController.reload)
router.delete('/url/:id', SiteController.remove)
router.post('/url/cookie', SiteController.uploadCookies)
router.get('/url/cookie', SiteController.getCount)

router.post('/send', EmailController.send)
router.post('/email', EmailController.create)
router.get('/email', EmailController.getAll)
router.delete('/email/:id', EmailController.remove)

router.get('/schedule', ScheduleController.getAll)
router.post('/schedule', ScheduleController.create)
router.delete('/schedule/:id', ScheduleController.remove)

router.post('/auth/register', AuthController.create)
router.post('/auth/login', AuthController.login)
router.post('/auth/logout', AuthController.logout)

module.exports = router
